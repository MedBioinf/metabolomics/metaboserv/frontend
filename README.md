# MetaboSERV

MetaboSERV is a web-based, open-source metabolomics platform based on Python, VueJS3, R and TypeScript.

## Installation

This installation guide covers both the [MetaboSERV frontend](https://gitlab.gwdg.de/MedBioinf/metabolomics/metaboserv/frontend) and the [MetaboSERV backend](https://gitlab.gwdg.de/MedBioinf/metabolomics/metaboserv/backend).
The frontend can not adequately be used without the backend.

MetaboSERV is recommended to be used with Docker or Podman. You can also install everything natively, but this will also not be covered here. We recommend a system with *at least* two cores and 6GB of RAM (but ideally more) to install MetaboSERV.
First, create a directory for MetaboSERV and download the required additional files [here](https://owncloud.gwdg.de/index.php/s/oZEH5eFMLrUXv4M). Put the compose file (*docker-compose.yml*) and the SQL script (*create_database.yml*) into the newly created directory, which will be referred to as the **"project root directory"** further on.

### Compose-file

The compose-file, *docker-compose.yml*, controls different parameters associated with the MetaboSERV application.
Unless your environment demands it, it is recommended to leave ports, service names, volume names and network names unchanged.

The following file paths are **mandatory** to adjust. On Windows, remember to double each backslash symbol, e.g. `C:\\\\some\\host\\path`.
- In the `metaboserv-backend:volumes` section, there are two host paths that need to be adapted. These host paths must be readable and writeable directories on your host system, which are then mounted into the MetaboSERV backend Docker container.
- The first one is mapped to the internal container path */app/files* . This resembles the directory where all experimental data uploaded to MetaboSERV will be saved. Set a sensible path on your host system here, such as */mnt/metaboserv* for external network storage (recommended, but local paths on the server will work fine).
- The second one is mapped to the internal container path */app/data*. It is intended for additional non-experimental data that MetaboSERV can use to provide extra functionality, for example HMDB reference concentrations. Set an according path on your host system here.
- In the `metaboserv-db:volumes` section, replace `/your/project/root/create_database.sql` with the path to the *create_database.sql* file you downloaded earlier, which should be in your project root directory. The reason this is necessary is that Docker prefers absolute host paths to relative ones.

The following parameters can **optionally** be adjusted:
- In the `metaboserv-es` section: `ES_JAVA_OPTS` for Elasticsearch - indicates how much RAM Elasticsearch will use. The default is 4GB, i.e. 4096MB.
- The MariaDB login information can be changed in the section `metaboserv-db:environment`. Make sure to adapt any changes in the configuration for the backend service later on.
- If you wish to use the Bruker NMR visualization service based on R, uncomment the `metaboserv-raw` section. You may notice there is another volume here, mapped to the internal path */app/files*. Please use the same directory you already used for */app/files* in the `metaboserv-backend:volumes` section.

### Frontend

1. In your terminal, navigate to the project root directory and clone the [frontend git project]( https://gitlab.gwdg.de/MedBioinf/metabolomics/metaboserv/frontend). You should now have a folder called *frontend* in your project root directory, navigate into it.
2. The frontend folder contains a file called *.env.template*. Copy this file and rename the copy to *.env*. This local copy will not be overwritten by updates in the repository. Edit the *.env* file and set the URL where you want/expect the backend service to be. By default, it is set to `http://localhost:9201`, so if the backend will run on the same machine as the frontend and you want to keep the default backend port (9201), you can skip this step - otherwise adapt the host and port according to your system. Note that you will have to use a public URL that can be resolved by a browser here in production.
3. Build the frontend container (from the according *frontend* directory): 
    ```bash
    docker build -t metaboserv-frontend .
    ```
  If you ever wish to change the value in the *.env* file, you will have to rebuild the Docker image using the same command.
4. You can also change the name of the frontend container, make sure to also change it in the provided `docker-compose.yml`.

### Backend

1. Navigate to the project root directory, clone the [backend git project]( https://gitlab.gwdg.de/MedBioinf/metabolomics/metaboserv/backend) and then navigate into the new *backend* folder.
2. The backend folder contains a file called *metaboserv_conf_docker_template.yml*. Copy this file and rename the copy to *metaboserv_conf_docker.yml*. This local copy will not be overwritten
by updates in the repository. Configure *metaboserv_conf_docker.yml* to your needs. Here is a quick rundown of your configuration options (anything not mentioned is recommended to be left unchanged):
  - In the `flask` section: It is recommended to leave the top four values unchanged. Definitely change `jwt_key`, as this is the string of characters used to generate JSON web tokens. If someone knows this key, they can generate login tokens for your MetaboSERV instance. In production, set `jwt_https` to true. You can change the JWT expiration time (`jwt_expiration`), which is given in hours, if you wish.
  - In the `backend` section, you should change the `admin_password`. This is the password for the *admin* account on MetaboSERVs web interface, which is created automatically and has special permissions. The parameter `enforce_hmdb` dictates whether or not HMDB identifiers will be enforced for your MetaboSERV instance. The other three parameters in this section should not be changed unless you wish to change the container-internal filepaths.
  - In the `elasticsearch` section, you can optionally enter the `password` for the *elastic* user in case you chose to enable this additional security measure and obtained the password earlier. In theory, you can also use an entirely different account from *elastic*, in which case you need to adapt `username` as well.
  - In the `mariadb` section, you should change the `password` to the same password that you set in `docker_compose.yml` for the MariaDB login information. There, it can be found in the section `metaboserv-db:environment` as the environment variable `MYSQL PASSWORD`.
  - The section `smtp` deals with mail verification. If you do not wish to use mail verification, simply set `verification` to False (the other SMTP parameters do not matter in this case). Otherwise, pass a valid function `email` address, `password`, SMTP `server` and SMTP server `port`. Independent of the verification system, you can also set the amount of failed login attempts to trigger an email notification (`notify_attempts`) or to trigger a temporary account lock (`notify_attempts`). If you do not wish to use a feature, simply set it to 0. Note that the `notify_attempts` feature does require the SMTP parameters, i.e. a function mail account, to be set.
3. Once you are satisfied with the configuration file, build the backend container (from the according `backend` folder):
```bash
docker build -t metaboserv-backend .
```
You can change the parameters in *metaboserv_conf_docker.yml* at any point, you will however need to rebuild the Docker image using the same command.
4. If you wish to use the Bruker NMR visualization service (and have already adapted `docker-compose.yml` accordingly), navigate back to the project root directory and clone the according GitLab project (*nmr-parser*) from [here]( https://gitlab.gwdg.de/MedBioinf/metabolomics/metaboserv/nmr-parser). Navigate into the new directory and build the service:
```bash
docker build -t metaboserv-raw .
```
5. Start up MetaboSERV by running `docker compose up -d`, or `docker-compose up -d` if you are running an older version of Docker. You can omit the `-d` flag if you don’t want the containers to run in the background. This will pull some Docker images (which requires internet access) if they are not already present. If your machine does not have internet access, consider the use of `docker save` and `docker load` to transfer them from a machine with internet to the desired one. Refer to the [Docker documentation](https://docs.docker.com/reference/cli/docker/image/save/) for more information.
6. Verify that all four (five if you chose to install the *nmr-parser*) containers are running: `docker ps`.
7. Now, some final configuration steps have to be performed. In your browser, navigate to the SwaggerUI/OpenAPI instance that comes with MetaboSERV. By default, this can be found at `http://localhost:9201/metaboserv-api/doc`. Scroll down to the `login` endpoint, open it and click *Try it out*. Where it says `string`, enter the username "admin" and the password you set in the backend configuration, respectively. Click *Execute*. Scroll down to the Response body, there should be a line like `"access_token": "XXXXX"`. Copy the long line of characters (without quotation marks).
8. Scroll all the way up. On the right side of the screen, there is an *Authorize* button. Click it, paste the character string you copied in here and click *Authorize* again. You are now acting as the admin account and have necessary permissions to perform administrative tasks.

9. The first endpoint, at the very top, is called `/ESDatabaseInitialize`. Click on it, then click *Try it out* and click *Execute*. This will initialize the Elasticsearch database.
10. MetaboSERV is now set up for use! You can also add HMDB reference concentrations and synonyms, which come in XML files you can download from [their FTP servers](https://hmdb.ca/downloads). Here is an example for the *serum_metabolites.xml* file, which for this purpose has to be put in the directory you specified during the configuration of the compose-file (the one mapped to */app/data*).
Once the HMDB file is in the correct folder, scroll down to the `CompoundAddFromHMDB` endpoint. Click it, then click *Try it out* and type the name of the HMDB file (e.g. *serum_metabolites.xml*) into the `filename` text field. Click *Execute*. This may take a while, but should eventually return a "Success" response.

### Uninstalling MetaboSERV

To uninstall MetaboSERV, you need to remove the Docker containers and volumes as well as any files associated with MetaboSERV stored on the host system. Bring up Docker images using the command `docker ps` which should show the four to five MetaboSERV images (depending on whether the NMR Parser was installed or not). Remove each image using the command `docker rmi [IMAGE-ID]` where `[IMAGE-ID]` is the identifier the image was saved under. Once all images are removed, use the command `docker system prune` to remove the volumes. Delete the directory where you installed MetaboSERV (making sure to back up any files you want to keep beforehand) to complete the process.

## Using MetaboSERV

This usage guide assumes that you either have access to a running MetaboSERV instance, or that you have successfully installed and set up both the MetaboSERV backend and the MetaboSERV frontend as well as the required databases yourself.

### Starting MetaboSERV

1. In the MetaboSERV project root directory, run `docker-compose up` (or `docker compose up`, based on your Docker version) to start up the backend and frontend. Optionally omit the `-d` flag in order to not have the Docker containers running in the background. Use `docker compose down` to shut down MetaboSERV, and `docker compose restart` to restart it.
2. Navigate to the according URL in your browser to access the MetaboSERV frontend. By default, this is `http://localhost:9202`. For production, i.e. to make your MetaboSERV instance accessible on the internet, you should use HTTPS and a reverse proxy.

### Logging in and out

Click on the button on the top right to log in (or log out, if you are already logged in). You can either use a MetaboSERV account, which you can register by clicking the link on the bottom of the login menu, or an authorization token that has been passed to you by a third party.

If verification is turned on in the configuration options, you may need to verify your account before you unlock most permissions. A verification mail will be sent to you in this case, which will contain a code you need to enter once in the "Account" section after registering your account.

Both variants of authentication (user accounts and authorization tokens) will allow you to browse, download and in some cases edit a set of studies which were uploaded to MetaboSERV. If you use a MetaboSERV account, you may upload new studies, as well.

You can change your password in the "Account" section as well. If you have ever forgotten your password, you can use the according option in the login interface to receive a new temporary password through your e-mail address.

### Creating/deleting/editing studies

Navigate to the "Studies" tab and click the plus. Fill in the mandatory metadata values on the left. On the right side, you can add annotated files, which must adhere to the [format specifications](#format) stated below. On the bottom right, you can also manually add some metadata entries.

You can tick the *transpose* option in order to swap rows and columns (refer to the [format specifications](#format)).

You can edit studies at any time by clicking on a study in the "Studies" tab and clicking on the "Edit" button. You can also delete studies here if you wish.

### Uploading/downloading experimental data

In the "Studies" tab, click on a study and click on "Manage experimental data". To upload files, you can either click on the dropbox on the left to browse your machine for according files, or drag files into the box using drag-and-drop. Uploaded files appear on the right, where you can download or delete them (given the necessary permissions). Note that the files you used to create the study (such as the concentration or phenotype data files) are automatically uploaded here.

Uploaded files can be used to alter the concentration and phenotype data or the study metadata at any time. Note that concentration data and phenotype data are always completely replaced if you use these options, while metadata you upload through a file is merged with existing metadata (with metadata from the file receiving priority in case of duplicate keys).

### Managing study permissions

Click on an existing study in the "Studies" tab. If you have the necessary permissions, you will be able to add new contributors, remove contributors (top left) and manage authorization tokens (top right) for the study. Otherwise, you will only be able to browse the contained phenotypes and metadata.

### Queries

You can query your studies by navigating to the "Query" tab. There are four distinct sections here, three of which open up after you selected at least one study in the **first section** ("Study Selection"). Any study you select here (indicated by being listed on the right) is included in your query.

In the **second section**, you can optionally limit the amount of metabolites to consider in your query. By default, all metabolites are used. On the very top of the window, you can choose to only retrieve a selected subset of metabolites, or to retrieve all metabolites but the selected subset. Select metabolites by clicking on them in the list on the left.

In the **third section**, you can optionally further limit which entries to consider by imposing filter rules on metabolites. There are generally three different filter rules. The first will cause your query to only include entries where the concentration level of the specified metabolite falls inside the range you provide. The second filter type does the same, but only considers entries where the concentration level falls outside of the provided range. The last only serves to visualize a particular range and will not change the query set.

If reference concentrations are contained in the MetaboSERV instance, you can also select them here wherever possible instead of providing a numerical range by yourself.

In the **fourth section**, you can choose which phenotypes should be included in your query. You can also limit the query set by the levels of each nominal phenotype, such as only considering healthy patients. 

### Creating quality control plots and heatmaps, visualizing Bruker NMR spectra

MetaboSERV facilitates easy creation of quality control plots and Pearson correlation heatmaps for your uploaded concentration data. Navigate to "Browse" and select a study you want to create a quality control plot. Then select your desired plot, choose whether you want to create a plot from a `<LOD`or `>LOD` perspective (for QC plots) and click on Submit.
Bruker NMR spectra can be visualized here, as well. Simply select a study, then select the according archive from the dropdown list and click on "Execute". Note that the archive has to be uploaded using the "Manage experimental metadata" functionality of the study beforehand.

### Mapping metabolites to their HMDB identifiers

Navigate to the MetaboSERV landing page. The bottom line of the first section (right above "Citation") has a link that will take you to the mapping utility.
Here, you can either map a list of metabolites to their HMDB equivalents or provide a file that follows the concentration data [format specifications](#format) outlined below. Click on the "Map" button. For each metabolite (either taken from your provided list or provided file), the MetaboSERV database will be searched (employing a *fuzzy matching* approach, i.e. metabolite names do not have to match exactly to be considered) for a fitting HMDB identifier.

All matches will have to be acknowledged and accepted by you, which you can do by clicking on the green tick button. Exact matches are accepted automatically. If you are dissatisfied with a particular mapping, you can browse through different matches for each metabolite by clicking on the `<` and `>` buttons. We use [The Chemical Translation Service](https://cts.fiehnlab.ucdavis.edu/) (CTS) provided by the Fiehn Lab at UC Davis West Coast Metabolomics Center to try and resolve metabolites that were not matched at all in the MetaboSERV database. In case no match can be found using the MetaboSERV database or the CTS, which is indicated by the word *Failed*, you can still manually research and provide a correct identifier by clicking on the `+` button. Note that this feature mostly exists for instances where HMDB identifiers are enforced - you can also simply accept failed matches or remove matches where you are unsatisfied with any of the suggestions (by clicking on the red `X`), which will result in the original metabolite name remaining and not being replaced by a HMDB identifier.
The resulting metabolite mapping can be downloaded in JSON or CSV format. If you provided a concentration data file, you can also download the adapted version with all metabolites being replaced with their HMDB accession numbers (as long as you accepted the mappings).

## Format specifications
<a name="format"></a>

For any data you upload, a particular format has to be adhered to in order for MetaboSERV to parse your files. The following sections describe the allowed formats in detail for each particular type of file, of which there are:
- Concentration data files, containing concentration levels of particular sources (e.g. patients) in your study
- Metadata files, containing general metadata for your study
- Phenotype files, containing nominal phenotype data for particular sources (e.g. patients) in your study
- Raw experimental data. This should be compressed/archived to save storage space, but otherwise no restrictions are placed on it.

Templates can be downloaded from the public MetaboSERV instance's landing page.

### Concentration data files

Allowed file extensions are: CSV, TSV, XLS, XLSX.
Allowed separators are tabs (`\t`) and semicolons (`;`). Even in CSV files, commas are currently not allowed in order not to interfere with the parsing of numerical data.
The standard layout for concentration data files is to have one column per metabolite, and one column containing according patient IDs. Each source, e.g. a patient, is then reflected in one row of the file.

Please provide a header row. The column containing patient IDs should contain a header called `id` or `source_id`. All other columns are considered as metabolites, and whatever you pass in the header row is considered the metabolite name. In order to make the most out of MetaboSERV's querying features, such as combining different studies containing similar metabolites in a single query, it is optimal to provide HMDB accession numbers instead of metabolite names as those are guaranteed to be matched. However, any metabolite names are accepted unless HMDB identifiers are enforced in the relevant MetaboSERV instance.

In addition to rows for each source, you may also pass a special row that indicates the unit used for each metabolite. Insert `unit` in the `source_id` column of the file for this row, and then provide the according unit for each metabolite in the respective columns. MetaboSERV places no restrictions on unit identifiers.

Refer to this exemplary table or to the provided exemplary files for clarification:

| source_id | metabolite 1 | metabolite 2 | ... |
| --------- | ------------ | ------------ | --- |
| unit      | uL           | mmol/L       | ... |
| patient_1 | 5471         | <LOD         | ... |
| patient_2 | 6901         | 279          | ... |
|    ...    |      ...     |      ...     | ... |

Each cell may contain either nothing, the special string `<LOD` (both treated as NAs) or a numerical value indicating the concentration value of the according source x for metabolite y.

You can also swap rows and columns, in which case you need to tick the *transpose* option for the concentration file when creating your study.

### Metadata files

Allowed file extensions: YML, JSON.
You can provide a simple YML file to add some additional metadata for your study. This is particularly useful if you have large amounts of metadata. Otherwise, it is recommended to simply set it using the frontend GUI during study creation.

Each metadata entry is associated with a unique ID, a name and a value, passed in the format
```yaml
id:
  name: Name
  value: Value 
```
Example:
```yaml
instrument:
  name: Spectrometer
  value: 600 MHz Bruker Avance III
another_md:
  name: Another attribute
  value: Another value
```
Or the JSON equivalent:
```json
{
  "instrument": {
    "name": "Spectrometer",
    "value": "600 MHz Bruker Avance III"
  },
  "another_md": {
    "name": "Another attribute",
    "value": "Another value"
  }
}
```

You can pass an arbitrary amount of entries this way.

### Phenotype data files

Allowed file extensions: CSV, TSV, XLS, XLSX. Allowed separators are tabs (`\t`) and semicolons (`;`). Even in CSV files, commas are currently not allowed in order not to interfere with the parsing of numerical data.
Generally, phenotype files are set up similarly to concentration data files. There are three types of possible columns:
- ID column (mandatory), which should be indicated by setting `id` or `source_id` in the header row. Add all source IDs, e.g. patient IDs, here.
- Phenotype columns (at least one is mandatory). Indicate phenotype columns by adding `PHENOTYPE_` before the phenotype description in the header row, e.g. `PHENOTYPE_Acute Kidney Injury`. Each cell in this column should contain the phenotype level that the source (e.g. patient) in each row is associated with.
- Metadata columns (optional). All columns which are not starting with `PHENOTYPE_` or are marked as the ID column are treated as metadata. You can add arbitrary values into these columns.

Refer to this exemplary table or the provided exemplary files for clarification:

| source_id | PHENOTYPE_some disease | PHENOTYPE_something else | collection time | ... |
| --------- | ---------------------- | ------------------------ | --------------- | --- |
| patient_1 | healthy                | level 1                  | 17:37pm         | ... |
| patient_2 | sick                   | level 1                  | 11:17am         | ... |
| patient_3 | healthy                | level 2                  |                 | ... |
|    ...    |           ...          |            ...           |        ...      | ... |

### Raw experimental data

MetaboSERV sets no limitations about the data you choose to upload to a study, i.e. all types of files are allowed. It is up to the user to make sure no illegal content is uploaded. However, we recommend compressing larger files in order to reduce the storage space allocated to your study.




