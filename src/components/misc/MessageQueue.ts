export class MessageQueue<APIResult> implements GenericQueue<APIResult> {
    private storage: APIResult[] = [];
  
    constructor(private capacity: number = 5) {}
  
    enqueue(item: APIResult): void {
      if (this.size() === this.capacity) {
        this.dequeue();
      }
      this.storage.push(item);
    }
    dequeue(): APIResult | undefined {
      return this.storage.shift();
    }
    size(): number {
      return this.storage.length;
    }
  }

  export interface APIResult {
    body?: Object | undefined,
    message?: string | undefined,
    status?: number|string | undefined,
}

export interface GenericQueue<APIResult> {
    enqueue(res: APIResult): void;
    dequeue(): APIResult | undefined;
    size(): number;
}