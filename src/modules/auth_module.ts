import { useAuthStore } from "@/stores/auth";
import { getStudyList } from "./study_module";

/**
 * Registers a new user account.
 * 
 * @param username - Username
 * @param password - Password (encrypted)
 * @param email - E-Mail Address
 * @param institution - Institution
 * @returns - Message (success, failure)
 */
export const register = async(username: string, password: string, email: string, institution: string) => {
    let response = await fetch(`${import.meta.env.VITE_BACKEND_PATH}register`, {
      method: 'post',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      body: JSON.stringify({username: username, password: password, email: email, institution: institution}),
    }).then( async (res) => {
      return await res.json()
    })
    return response;
  }

/**
 * Logs in a user if username and password match the database.
 * 
 * @param username - Username provided by the user
 * @param password - Password provided by the user (encrypted)
 * @returns Login message (Success, failure)
 */
export const login = async(username: string, password: string) => {
    let response = await fetch(`${import.meta.env.VITE_BACKEND_PATH}login`, {
      method: 'post',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      body: JSON.stringify({username: username, password: password}),
    }).then( async (res) => {
      return await res.json()
    })
    if (response.status == 200) {
      const auth = useAuthStore();
      auth.jwt = response.access_token;
      auth.username = response.username;
      auth.aut = null;
      window.sessionStorage.setItem('METABOSERV-JWT', <string>auth.jwt);
      window.sessionStorage.setItem('METABOSERV-USER', <string>auth.username);
      window.sessionStorage.removeItem('METABOSERV-AUT');
   }
   return response;
}

/**
 * Generates headers for communication with the flask backend.
 * Considers JWT tokens (set through cookies) or general access tokens (provided through URL)
 * 
 * @param content_type - Content Type of the HTTP body
 * @param accept - Accepted content type for the header
 * @returns Headers with (optional) content-type, (optional) accept and auth token
 */
export const generateHeaders = (content_type?: string, accept?: string) => {
    const auth = useAuthStore();
    checkSessionStorage();
    let header_obj: HeadersInit = new Headers();
    if (content_type) header_obj.set('Content-Type', content_type);
    if (accept) header_obj.set('Accept', accept);
    if (auth.jwt) header_obj.set('Authorization',`Bearer ${auth.jwt}`);
    if (auth.aut) header_obj.set('METABOSERV-AUTH-TOKEN', auth.aut);
    return header_obj;
}

/**
 * Logs out the user.
 */
export const logout = () => {
  const auth = useAuthStore();
  auth.jwt = null;
  auth.username = "";
  auth.aut = null;
  window.sessionStorage.removeItem('METABOSERV-JWT');
  window.sessionStorage.removeItem('METABOSERV-USER');
  window.sessionStorage.removeItem('METABOSERV-AUT');
}

/**
 * Checks whether a JWT and username are already present.
 */
const checkSessionStorage = () => {
  let potential_jwt: string | null = window.sessionStorage.getItem('METABOSERV-JWT');
  let potential_user: string | null = window.sessionStorage.getItem('METABOSERV-USER');
  let potential_aut: string | null = window.sessionStorage.getItem('METABOSERV-AUT');
  if (potential_jwt != null && potential_user != null) {
    const auth = useAuthStore();
    auth.jwt = potential_jwt;
    auth.username = potential_user;
    auth.aut = null;
    window.sessionStorage.removeItem('METABOSERV-AUT');
  } else if (potential_aut != null && potential_user == 'Auth-Token User') {
    const auth = useAuthStore();
    auth.jwt = null;
    auth.aut = potential_aut;
    auth.username = potential_user;
    window.sessionStorage.removeItem('METABOSERV-JWT');
  }
}

/**
 * Logs in a user if username and password match the database.
 * 
 * @param token - Authorization token
 * @returns Login message (Success, failure)
 */
export const token_login = async(token: string) => {
  let response = await fetch(`${import.meta.env.VITE_BACKEND_PATH}TokenLogin`, {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    },
    body: JSON.stringify({token_id: token}),
  }).then( async (res) => {
    return await res.json()
  })
 if (response.status == 200) {
  const auth = useAuthStore();
  auth.aut = token;
  auth.jwt = null;
  window.sessionStorage.setItem('METABOSERV-AUT', <string>auth.aut);
  window.sessionStorage.setItem('METABOSERV-USER', "Auth-Token User");
  window.sessionStorage.removeItem('METABOSERV-JWT');
  await getStudyList();
 }
 return response;
}

/**
 * Creates a new authorization token
 * @param studies - The studies to create the token for
 * @param auth_level - The level of authorization to grant through the token
 * @param expiration - Number of days after which the token expires (<=0: never expires)
 * @returns Response object
 */
export const createToken = async(studies: string[], auth_level?: string, expiration?: number, comment?: string) => {
  const token_form: FormData = new FormData();
  token_form.append('study', studies.join(';'));
  if (auth_level) token_form.append('auth_level', auth_level);
  if (comment) token_form.append('comment', comment);
  if (expiration) token_form.append('expiration', expiration.toString())
  let response = await fetch(`${import.meta.env.VITE_BACKEND_PATH}TokenCreate`, {
    method: 'post',
    headers: generateHeaders(),
    body: token_form,
  })
  return response.json();
}

/**
 * Retrieves a list of tokens for a particular study.
 * @param study_id - The study in question
 */
export const retrieveTokens = async(study_id: string) => {
  let params = new URLSearchParams({
    'study_id': study_id,
  });
  let result = await fetch(`${import.meta.env.VITE_BACKEND_PATH}TokenListForStudy?` + params, {
    method: 'get',
    headers: generateHeaders('application/json')
});
if (result.status == 200) {
    let result_json = await result.json();
    return result_json.body;
}
return null;
}

/**
 * Retrieves user data for the user currently logged in.
 */
export const retrieveUserData = async() => {
  const auth = useAuthStore();
  let params = new URLSearchParams({
    'username': auth.username,
  });
  let result = await fetch(`${import.meta.env.VITE_BACKEND_PATH}UserGet?` + params, {
    method: 'get',
    headers: generateHeaders('application/json')
});
if (result.status == 200) {
    let result_json = await result.json();
    return result_json.body;
}
return null;
}

/**
 * Revokes (deletes) an authorization token
 * @param token - The token in question
 * @returns Response object
 */
export const revokeToken = async(token: string) => {
  const token_form: FormData = new FormData();
  token_form.append('token_id', token);
  let response = await fetch(`${import.meta.env.VITE_BACKEND_PATH}TokenRevoke`, {
    method: 'post',
    headers: generateHeaders(),
    body: token_form,
  })
  return response.json();
}

/**
 * Attempts verification for the logged in user
 * @param code - Verification code
 * @returns Response object
 */
export const verifyUser = async(code: string) => {
  const auth = useAuthStore();
  const token_form: FormData = new FormData();
  token_form.append('username',auth.username);
  token_form.append('code', code);
  let response = await fetch(`${import.meta.env.VITE_BACKEND_PATH}UserVerify`, {
    method: 'post',
    headers: generateHeaders(),
    body: token_form,
  })
  return response.json();
}

/**
 * Resets password for a user
 * @param username - User account
 * @returns Response object
 */
export const sendRecoveryEmail = async(username: string) => {
  const token_form: FormData = new FormData();
  token_form.append('username', username);
  let response = await fetch(`${import.meta.env.VITE_BACKEND_PATH}UserSendPasswordForgottenCode`, {
    method: 'post',
    headers: generateHeaders(),
    body: token_form,
  })
  return response.json();
}

/**
 * Resets password for a user
 * @param username - User account
 * @returns Response object
 */
export const recoverPassword = async(username: string, code: string) => {
  const token_form: FormData = new FormData();
  token_form.append('username', username);
  token_form.append('code', code)
  let response = await fetch(`${import.meta.env.VITE_BACKEND_PATH}UserEnterPasswordForgottenCode`, {
    method: 'post',
    headers: generateHeaders(),
    body: token_form,
  })
  return response.json();
}

/**
 * Deletes account of the logged in user
 * @returns Response object
 */
export const deleteUser = async() => {
  const auth = useAuthStore();
  const token_form: FormData = new FormData();
  token_form.append('username',auth.username);
  let response = await fetch(`${import.meta.env.VITE_BACKEND_PATH}UserDelete`, {
    method: 'post',
    headers: generateHeaders(),
    body: token_form,
  })
  return response.json();
}

/**
 * Changes password for an account
 * @param old_password - Old password
 * @param new_password - New password
 * @returns Response object
 */
export const changePassword = async(old_password: string, new_password: string) => {
  const auth = useAuthStore();
  const token_form: FormData = new FormData();
  token_form.append('username',auth.username);
  token_form.append('old_password', old_password);
  token_form.append('new_password', new_password);
  let response = await fetch(`${import.meta.env.VITE_BACKEND_PATH}UserChangePassword`, {
    method: 'post',
    headers: generateHeaders(),
    body: token_form,
  })
  return response.json();
}

/**
 * Retrieves a list of tokens for a particular study.
 * @param study_id - The study in question
 */
export const tokenIsContributor = async(study_id: string) => {
  let params = new URLSearchParams({
    'study_id': study_id,
  });
  let result = await fetch(`${import.meta.env.VITE_BACKEND_PATH}TokenIsContributor?` + params, {
    method: 'get',
    headers: generateHeaders('application/json')
});
if (result.status == 200) {
    let result_json = await result.json();
    console.log(result_json)
    return result_json.value == "true";
}
return false;
}




export type Entries<T> = {
  [K in keyof T]: [K, T[K]];
}[keyof T][];

export interface TokenData {
  token_id: string,
  creator: string,
  date_created: string,
  date_expiration?: string,
  auth_level: string,
  comment?: string,
  studies?: string[],
}

export interface UserData {
  username: string,
  role: string,
  studies: object,
  email: string,
  institution?: string,
  verified: string,
  verification_code?: string,
  verification_expiration?: string,
  attempted_logins?: object,
}
  
  
  