import type { ArbitraryObject } from "@/stores/query";
import { generateHeaders } from "./auth_module";
import { useQueryStore } from "@/stores/query";
import { useMessageStore } from "@/stores/message";

/**
 * GETs data on the contained studies from flask microservice.
 * @returns JS Object
 */
export const getNconcs = async (compound_id: string): Promise<NormalConcentration[]|null> => {
    let result = await fetch(`${import.meta.env.VITE_BACKEND_PATH}CompoundGetNconcs?` + new URLSearchParams({
        'compound_id': compound_id,
    }), {
        method: 'get',
        headers: generateHeaders('application/json')
    });
    if (result.status == 200) {
        let result_json = await result.json();
        return result_json.body as NormalConcentration[];
    }
    return null;
}

/**
 * Executes a complete query to MetaboSERV, gathering concentration and phenotype data in the process.
 * @param studies - The studies to gather data for
 * @param compounds - Compound selection to retrieve. If this is undefined, all are retrieved.
 * @param filters - Set of metabolite filter rules the results have to adhere to.
 * @param phenotypes - Set of phenotype filter rules the results have to adhere to.
 * @returns JS Object
 */
export const executeQuery = async (studies: string[], compounds?: string[], filters?: MetaboliteFilterRule[],
     phenotypes?: PhenotypeFilterRule[], visualizations?: MetaboliteFilterRule[], aggregate_multiple?: boolean): Promise<any> => {
    let params = new URLSearchParams({
        'studies': JSON.stringify(studies),
    });
    compounds && params.append("compounds", JSON.stringify(compounds));
    filters && params.append("filters", JSON.stringify(filters));
    phenotypes && params.append("phenotypes", JSON.stringify(phenotypes));
    visualizations && params.append("vis_ranges", JSON.stringify(visualizations))
    params.append("aggregate", aggregate_multiple ? "true" : "false");
    let result = await fetch(`${import.meta.env.VITE_BACKEND_PATH}MetaboservQuery?` + params, {
        method: 'get',
        headers: generateHeaders('application/json')
    });
    if (result.status == 200) {
        let result_json = await result.json();
        return result_json.body;
    } else if (result.status == 500) {
        
    }
    return null;
}

/**
 * GETs meta-information (name, levels) on phenotypes of a particular study.
 * @param study_id - The study in question
 * @returns JS Object
 */
export const getPhenotypeInformation = async (study_id: string): Promise<any> => {
    let result = await fetch(`${import.meta.env.VITE_BACKEND_PATH}PhenotypeGetInformation?` + new URLSearchParams({
        'study_id': study_id,
    }), {
        method: 'get',
        headers: generateHeaders('application/json')
    });
    if (result.status == 200) {
        let result_json = await result.json();
        return result_json.body;
    }
    return {};
}

export const retrievePlot = async (id: string) => {
    let result_url = await fetch(`${import.meta.env.VITE_BACKEND_PATH}PlotRetrieve?` + new URLSearchParams({
        'plot_id': id,
    }), {
        method: 'get',
        headers: generateHeaders('image/png')
    })
    .then(res => {
        if (res.status == 200) {
            return res.blob()}
        else if (res.status == 400) {
            const msg = useMessageStore();
            msg.add_message({msg: "Sorry, at least one plot could not be drawn \
                 - the cause could be the selected studies having different units for the same metabolites!", status:400});
        }
        else {
            throw new Error("No plot found for ID.")
        }})
    .then(blob => {
        if (!blob) return;
        return URL.createObjectURL(blob);
    }).catch(error => {
        return;
    });
    return result_url
}

function dec2hex (dec: number) {
    return dec.toString(16).padStart(2, "0")
}

function generateId (len: number) {
    var arr = new Uint8Array((len || 40) / 2)
    window.crypto.getRandomValues(arr)
    return Array.from(arr, dec2hex).join('')
}

/**
 * Generates a scatter plot, featuring two metabolites. Uses query data.
 * @param metabolite1 - Metabolite 1 (x-axis)
 * @param metabolite2 - Metabolite 2 (y-axis)
 * @param phenotype - Phenotype to plot, optional
 * @param visualizations - Concentration ranges to highlight
 * @returns Plot URL if successful
 */
export const generateScatterplotFromData = async (metabolite1: string, metabolite2: string, phenotype?: string, visualizations?: MetaboliteFilterRule[], units?: ArbitraryObject) => {
    const query = useQueryStore();
    let plot_form = new FormData();
    let plot_id = generateId(16);
    let result_subset: ArbitraryObject = query.metaboliteSubset(metabolite1, metabolite2);
    plot_form.append('plot_id', plot_id);
    plot_form.append('plot_data', JSON.stringify(result_subset));
    plot_form.append('plot_m1', metabolite1);
    plot_form.append('plot_m2', metabolite2);
    units && plot_form.append('units', JSON.stringify(units));
    visualizations && plot_form.append('vis_ranges', JSON.stringify(visualizations));
    phenotype && plot_form.append('plot_phenotype', phenotype);
    let plot_url = await fetch(`${import.meta.env.VITE_BACKEND_PATH}PlotCreateScatter`, {
        method: 'post',
        body: plot_form,
    }).then(res => {
        switch(res.status) {
            case 200:
                return plot_id;
        }
    })
    if (plot_url) {
        return plot_url;
    }
}

/**
 * Generates a histogram, featuring concentration of one metabolite. Uses query data.
 * @param metabolite - Metabolite to plot
 * @param phenotype - Phenotype to plot, optional
 * @param visualizations - Concentration ranges to highlight
 * @param units - Mapping of metabolites to particular units
 * @param histstyle - Style of the histogram
 * @param bins - Number of bins
 * @returns Plot URL if successful
 */
export const generateHistogramFromData = async (metabolite: string, phenotype?: string, visualizations?: MetaboliteFilterRule[], units?: ArbitraryObject, histstyle?: string, bins?: number) => {
    const query = useQueryStore();
    let plot_form = new FormData();
    let plot_id = generateId(16);
    let result_subset: ArbitraryObject = query.metaboliteSubset(metabolite);
    plot_form.append('plot_id', plot_id);
    plot_form.append('plot_data', JSON.stringify(result_subset));
    plot_form.append('plot_m1', metabolite);
    units && plot_form.append('units', JSON.stringify(units));
    visualizations && plot_form.append('vis_ranges', JSON.stringify(visualizations));
    phenotype && plot_form.append('plot_phenotype', phenotype);
    histstyle && plot_form.append('plot_histstyle', histstyle);
    bins && plot_form.append('plot_bins', bins.toString());
    let plot_url = await fetch(`${import.meta.env.VITE_BACKEND_PATH}PlotCreateHistogram`, {
        method: 'post',
        body: plot_form,
    }).then(res => {
        switch(res.status) {
            case 200:
                return plot_id;
        }
    })
    if (plot_url) {
        return plot_url;
    }

}

/**
 * Generates a correlation plot ("heatmap") depicting the pearson correlation between metabolites.
 * Uses query data.
 * @returns Plot URL if successful
 */
export const generateCorrelationPlotFromData = async () => {
    const query = useQueryStore();
    let plot_form = new FormData();
    let plot_id = generateId(16);
    plot_form.append('plot_id', plot_id);

    let plot_url = await fetch(`${import.meta.env.VITE_BACKEND_PATH}PlotCreateCorrelations`, {
        method: 'post',
        body: plot_form,
    }).then(res => {
        switch(res.status) {
            case 200:
                return plot_id;
        }
    })
    if (plot_url) {
        return plot_url;
    }
}

/**
 * Generates a plot from a Bruker spectrum.
 * @param study_id - The study in question
 * @param filename - The file (archive) to plot
 * @returns Plot URL if successful
 */
export const generateBrukerPlot = async (study_id: string, filename: string) => {
    const query = useQueryStore();
    let plot_form = new FormData();
    let plot_id = generateId(16);
    plot_form.append('plot_id', plot_id);
    plot_form.append('filename', filename);
    plot_form.append('study_id', study_id);

    let plot_url = await fetch(`${import.meta.env.VITE_BACKEND_PATH}PlotBrukerSpectrum`, {
        method: 'post',
        body: plot_form,
        headers: generateHeaders(),
    }).then(res => {
        switch(res.status) {
            case 200:
                return plot_id;
        }
    })
    if (plot_url) {
        return plot_url;
    }
}


/**
 * Generates a correlation plot ("heatmap") depicting the pearson correlation between metabolites.
 * Does not use query data, but requires the according study ID.
 * @param study_id - ID of the study to plot a heatmap for
 * @returns Plot URL if successful
 */
export const generateCorrelationPlotFromScratch = async (study_id: string) => {
    if (!study_id) return;
    let plot_form = new FormData();
    let plot_id = generateId(16);
    plot_form.append('plot_id', plot_id);
    plot_form.append('study_id', study_id);

    let plot_url = await fetch(`${import.meta.env.VITE_BACKEND_PATH}PlotCreateCorrelationsFC`, {
        method: 'post',
        headers: generateHeaders(),
        body: plot_form,
    }).then(res => {
        switch(res.status) {
            case 200:
                return plot_id;
                break;
            default:
                const msg = useMessageStore();
                msg.add_message({status: res.status, msg: "Sorry, something went wrong while creating the correlation plot!"});
        }
    })
    if (plot_url) {
        return plot_url;
    }
}

/**
 * Generates a quality control plot using query data
 * @param over_lod - If true, will depict samples > LOD, otherwise < LOD (or rather, NAs)
 * * @param study_id - ID of the study to plot a QC plot for
 * @returns Plot URL if successful
 */
export const generateQCPlotMetabolitesFromData = async (over_lod:string = "true", study_id: string = "") => {
    if (!study_id || study_id === "") return;
    const query = useQueryStore();
    let plot_form = new FormData();
    let plot_id = generateId(16);
    plot_form.append('plot_id', plot_id);
    plot_form.append('study_id', study_id);
    plot_form.append('over_lod', over_lod);
    plot_form.append('plot_data', JSON.stringify(query.query_results));

    let plot_url = await fetch(`${import.meta.env.VITE_BACKEND_PATH}QCPlotCreateMetabolite`, {
        method: 'post',
        body: plot_form,
    }).then(res => {
        switch(res.status) {
            case 200:
                return plot_id;
        }
    })
    if (plot_url) {
        return plot_url;
    }
}

/**
 * Generates a quality control plot (metabolite-based) from scratch, i.e. without performing a prior query.
 * @param over_lod - If true, will depict samples > LOD, otherwise < LOD (or rather, NAs)
 * @param study_id - ID of the study to plot a QC plot for
 * @returns Plot URL if successful
 */
export const generateQCPlotMetabolitesFromScratch = async (over_lod:string = "true", study_id: string) => {
    if (!study_id) return;
    let plot_form = new FormData();
    let plot_id = generateId(16);
    plot_form.append('plot_id', plot_id);
    plot_form.append('study_id', study_id);
    plot_form.append('over_lod', over_lod);

    let plot_url = await fetch(`${import.meta.env.VITE_BACKEND_PATH}QCPlotCreateMetaboliteFC`, {
        method: 'post',
        headers: generateHeaders(),
        body: plot_form,
    }).then(res => {
        switch(res.status) {
            case 200:
                return plot_id;
        }
    })
    if (plot_url) {
        return plot_url;
    }
}

/**
 * Generates a quality control plot (source-based) from scratch, i.e. without performing a prior query.
 * @param over_lod - If true, will depict samples > LOD, otherwise < LOD (or rather, NAs)
 * @param study_id - ID of the study to plot a QC plot for
 * @returns Plot URL if successful
 */
export const generateQCPlotSourcesFromScratch = async (over_lod:string = "true", study_id: string) => {
    if (!study_id) return;
    let plot_form = new FormData();
    let plot_id = generateId(16);
    plot_form.append('plot_id', plot_id);
    plot_form.append('study_id', study_id);
    plot_form.append('over_lod', over_lod);

    let plot_url = await fetch(`${import.meta.env.VITE_BACKEND_PATH}QCPlotCreateSourceFC`, {
        method: 'post',
        headers: generateHeaders(),
        body: plot_form,
    }).then(res => {
        switch(res.status) {
            case 200:
                return plot_id;
        }
    })
    if (plot_url) {
        return plot_url;
    }
}

export const areHMDBIdentifiersEnforced = async() => {
    let result = await fetch(`${import.meta.env.VITE_BACKEND_PATH}IsHMDBEnforced`, {
        method: 'get',
        headers: generateHeaders('application/json')
    });
    return result.json();
}

export const mapMetaboliteToHMDB = async(to_map: string) => {
    let result = await fetch(`${import.meta.env.VITE_BACKEND_PATH}CompoundsToHMDB?` + new URLSearchParams({
        'compounds': to_map,
    }), {
        method: 'get',
        headers: generateHeaders('application/json')
    });
    return result.json();
}

export const nextMetabolite = async(compound: string, hmdb: string) => {
    let result = await fetch(`${import.meta.env.VITE_BACKEND_PATH}NextCompound?` + new URLSearchParams({
        'compound': compound,
        'hmdb_accession': hmdb
    }), {
        method: 'get',
        headers: generateHeaders('application/json')
    });
    return result.json();
}

export const previousMetabolite = async(compound: string, hmdb: string) => {
    let result = await fetch(`${import.meta.env.VITE_BACKEND_PATH}PreviousCompound?` + new URLSearchParams({
        'compound': compound,
        'hmdb_accession': hmdb
    }), {
        method: 'get',
        headers: generateHeaders('application/json')
    });
    return result.json();
}

export const retrieveMetaboliteNamesFromFile = async(compound_file: File, transpose: boolean) => {
    if (!compound_file) return;
    let file_form = new FormData();
    file_form.append('conc_file', compound_file)
    file_form.append('conc_transpose', transpose ? "true" : "false");

    const metabolites = await fetch(`${import.meta.env.VITE_BACKEND_PATH}CompoundGetMetabolitesFromFile`, {
        method: 'post',
        headers: generateHeaders(),
        body: file_form,
    });
    return metabolites.json();
}

export const serveMappedMetabolitesAsJSON = async (results: ArbitraryObject) => {
    if (!results) return;

    const metabolites = await fetch(`${import.meta.env.VITE_BACKEND_PATH}CompoundMappedAsJSON`, {
        method: 'post',
        headers: generateHeaders('application/json'),
        body: JSON.stringify({map_results: JSON.stringify(results)}),
    }).then((res) => {
        if (res.status == 200) {
            return res.blob();
        } else {
            window.alert("Sorry, there was a problem providing your download!");
            return null;
        }
    });
    return metabolites;
}

export const serveMappedMetabolitesAsCSV = async (results: ArbitraryObject) => {
    if (!results) return;

    const metabolites = await fetch(`${import.meta.env.VITE_BACKEND_PATH}CompoundMappedAsCSV`, {
        method: 'post',
        headers: generateHeaders('application/json'),
        body: JSON.stringify({map_results: JSON.stringify(results)}),
    }).then((res) => {
        if (res.status == 200) {
            return res.blob();
        } else {
            window.alert("Sorry, there was a problem providing your download!");
            return null;
        }
    });
    return metabolites;
}

export const serveMappedMetabolitesAdaptedFile = async(compound_file: File, adapted_metabolites: ArbitraryObject, transpose: boolean) => {
    if (!compound_file || !adapted_metabolites) return;
    const cf_form = new FormData();
    cf_form.append('adapted_metabolites', JSON.stringify(adapted_metabolites));
    cf_form.append('conc_file', compound_file);
    cf_form.append('transpose', transpose ? "true": "false")
    const metabolites = await fetch(`${import.meta.env.VITE_BACKEND_PATH}CompoundMappedAsAdaptedFile`, {
        method: 'post',
        headers: generateHeaders(),
        body: cf_form,
    }).then((res) => {
        if (res.status == 200) {
            return res.blob();
        } else {
            window.alert("Sorry, there was a problem providing your download!");
            return null;
        }
    });
    return metabolites;
}

export const queryResultsToCSV = async () => {
    const query = useQueryStore();

    const csv = await fetch(`${import.meta.env.VITE_BACKEND_PATH}QueryResultsToCSV`, {
        method: 'post',
        headers: generateHeaders('application/json'),
        body: JSON.stringify({query_results: query.query_results}),
    }).then((res) => {
        if (res.status == 200) {
            return res.blob();
        } else {
            window.alert("Sorry, there was a problem providing your download!");
            return null;
        }
    });
    return csv;
}

export const deleteTempFile = async (filename: string) => {
    let fd = new FormData();
    fd.append('filename', filename);
    const csv = await fetch(`${import.meta.env.VITE_BACKEND_PATH}DeleteTempFile`, {
        method: 'post',
        headers: generateHeaders('application/json'),
        body: fd,
    });
}

export interface NormalConcentration {
    compound_id: string,
    nconc_min: number,
    nconc_max?: number,
    nconc_unit?: string,
    nconc_state?: string,
    nconc_source?: string,
    nconc_age?: string,
    nconc_sex?: string,
    nconc_ref?: string,
    nconc_biospecimen: string,
}

export interface NormalConcentrationCollection {
    [key: string]: NormalConcentration[] | null,
}

export interface NormalConcentrationQuery {
    [key: string]: NormalConcentration,
}

export interface CompoundData {
    compound_id?: string,
    compound_formula?: string,
    compound_name?: string,
    compound_iupac?: string,
    compound_mol_weight?: string | number,
    compound_synonyms?: string[],
    hmdb_accession?: string,
    taxonomy?: {kingdom?: string, superclass?: string, group?: string},
}

export interface MetaboliteFilterRule {
    compound_id: string,
    filter_type: string,
    min: number,
    max?: number,
    unit: string,
}

export interface PhenotypeFilterRule {
    phenotype: string,
    include: boolean,
    included_levels?: string[],
}