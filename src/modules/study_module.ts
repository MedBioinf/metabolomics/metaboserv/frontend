import { useAuthStore } from "@/stores/auth";
import { generateHeaders } from "./auth_module";
import { useStudyStore, type StudyData, type StudyFiles } from "@/stores/studies";
import type { ArbitraryObject } from "@/stores/query";

/**
 * GETs data on the contained studies from flask microservice.
 * @returns JS Object
 */
export const getStudyList = async (): Promise<any> => {
    let result = await fetch(`${import.meta.env.VITE_BACKEND_PATH}StudyListAll`, {
        method: 'get',
        headers: generateHeaders('application/json')
    });
    if (result.status == 200) {
        const studies = useStudyStore();
        const auth = useAuthStore();
        let result_json = await result.json();

        let owned_studies = [] as StudyData[];
        let shared_studies = [] as StudyData[];
        let public_studies = [] as StudyData[];

        // Distribute studies into owned, shared and public
        Object.keys(result_json.body).forEach((study) => {
            if (!auth.username) public_studies.push(result_json.body[study]);
            else if (Object.keys(result_json.body[study]['study_members']).includes(auth.username) || Object.keys(result_json.body[study]['study_auth_tokens'])) {
                if (result_json.body[study]['study_members'][auth.username] == "uploader") {
                    owned_studies.push(result_json.body[study]);
                } else {
                    shared_studies.push(result_json.body[study]);
                }
            } else {
                public_studies.push(result_json.body[study]);
            }
        });

        studies.distributeStudies(owned_studies, shared_studies, public_studies);
    }
}

/**
 * Issues the creation of a new study.
 * 
 * @param study_data - The overall data for the study
 * @param study_files - Files for the study
 * @returns Response object
 */
export const createStudy = async (study_data: StudyData, study_files: StudyFiles, parse_params: ParseParameters) => {
    const study_form: FormData = new FormData();
    study_form.append('study_id', study_data.study_id);
    study_form.append('study_name', study_data.study_name);
    study_form.append('study_visibility', study_data.study_visibility);
    if (study_data.study_specimen) study_form.append('study_specimen', study_data.study_specimen);
    if (study_data.study_date) study_form.append('study_date', study_data.study_date);
    if (study_data.study_type) study_form.append('study_type', study_data.study_type);
    if (study_data.study_author) study_form.append('study_author', study_data.study_author);
    if (study_data.study_metadata) study_form.append('study_metadata', JSON.stringify(study_data.study_metadata));
    
    if (study_files.concentration_file) study_form.append('concentration_file', study_files.concentration_file);
    if (study_files.metadata_file) study_form.append('metadata_file', study_files.metadata_file);
    if (study_files.phenotype_file) study_form.append('phenotype_file', study_files.phenotype_file);

    if (parse_params.conc_delimiter) study_form.append('conc_delimiter', parse_params.conc_delimiter);
    if (parse_params.pheno_delimiter) study_form.append('pheno_delimiter', parse_params.pheno_delimiter);
    if (parse_params.conc_transpose != undefined) study_form.append('conc_transpose', (parse_params.conc_transpose ? 'true' : 'false'));
    if (parse_params.pheno_transpose != undefined) study_form.append('pheno_transpose', (parse_params.pheno_transpose ? 'true' : 'false'));
    let response = await fetch(`${import.meta.env.VITE_BACKEND_PATH}StudyCreate`, {
      method: 'post',
      headers: generateHeaders(),
      body: study_form,
    })
    return response.json();
}

/**
 * Updates a study and its metadata
 * @param study_id - Study to update
 * @param update_data - Update object containing key:value pairs to update
 * @returns Response object
 */
export const updateStudy = async (study_id: string, update_data: ArbitraryObject) => {
    const update_form: FormData = new FormData();
    update_form.append('study_id', study_id);
    update_form.append('data', JSON.stringify(update_data));
    let response = await fetch(`${import.meta.env.VITE_BACKEND_PATH}StudyUpdate`, {
        method: 'post',
        headers: generateHeaders(),
        body: update_form,
      })
    return response.json();
}

/**
 * GETs data on the contained studies from flask microservice.
 * @returns JS Object
 */
export const getStudyCompounds = async (studies: string[] | undefined): Promise<any> => {
    if ((!studies) || (studies.length == 0)) return {};
    let result = await fetch(`${import.meta.env.VITE_BACKEND_PATH}StudyGetCompounds?`  + new URLSearchParams({
        'studies': studies.join(';'),
    }),
    {
        method: 'get',
        headers: generateHeaders('application/json')
    });
    if (result.status == 200) {
        return result.json()
    }
    return {};
}

/**
 * GETs units for studies from the flask microservice.
 * @returns JS Object
 */
export const getStudyUnits = async (studies: string[] | undefined): Promise<any> => {
    if ((!studies) || (studies.length == 0)) return {};
    let result = await fetch(`${import.meta.env.VITE_BACKEND_PATH}StudyGetUnits?`  + new URLSearchParams({
        'studies': studies.join(';'),
    }),
    {
        method: 'get',
        headers: generateHeaders('application/json')
    });
    if (result.status == 200) {
        return result.json()
    }
    return {};
}

/**
 * GETs data on the contained studies from flask microservice.
 * @returns JS Object
 */
export const getStudyPhenotypes = async (studies: string[] | undefined): Promise<any> => {
    if (!studies) return {};
    let result = await fetch(`${import.meta.env.VITE_BACKEND_PATH}PhenotypeGetMultiple?`  + new URLSearchParams({
        'studies': studies.join(';'),
    }),
    {
        method: 'get',
        headers: generateHeaders('application/json')
    });
    if (result.status == 200) {
        return result.json()
    }
    return {};
}

/**
 * Updates a user, giving or revoking permissions.
 * @param study - The study in question
 * @param username - Name of the user to grant privileges to or revoke them from
 * @param level - Either "viewer", "contributor" (grants) or "revoke" ( removes).
 * @returns Response object
 */
export const updateUser = async(study: string, username: string, level: string,) => {
    const update_form: FormData = new FormData();
    update_form.append('study_id', study);
    update_form.append('username', username);
    update_form.append('level', level);

    let response = await fetch(`${import.meta.env.VITE_BACKEND_PATH}UserUpdate`, {
      method: 'post',
      headers: generateHeaders(),
      body: update_form,
    })
    return response.json();
}

/**
 * Updates a user, giving or revoking permissions.
 * @param study - The study in question
 * @param username - Name of the user to grant privileges to or revoke them from
 * @param level - Either "viewer", "contributor" (grants) or "revoke" ( removes).
 * @returns Response object
 */
export const addConcDataFromFile = async(study: string, filename: string, transpose: boolean) => {
    const update_form: FormData = new FormData();
    update_form.append('study_id', study);
    update_form.append('filename', filename);
    update_form.append('transpose', transpose ? "true" : "false");
    let response = await fetch(`${import.meta.env.VITE_BACKEND_PATH}AddConcentrationDataFromFile`, {
      method: 'post',
      headers: generateHeaders(),
      body: update_form,
    })
    return response.json();
}

/**
 * Updates a user, giving or revoking permissions.
 * @param study - The study in question
 * @param username - Name of the user to grant privileges to or revoke them from
 * @param level - Either "viewer", "contributor" (grants) or "revoke" ( removes).
 * @returns Response object
 */
export const addMetaDataFromFile = async(study: string, filename: string) => {
    const update_form: FormData = new FormData();
    update_form.append('study_id', study);
    update_form.append('filename', filename);
    let response = await fetch(`${import.meta.env.VITE_BACKEND_PATH}AddMetadataFromFile`, {
      method: 'post',
      headers: generateHeaders(),
      body: update_form,
    })
    return response.json();
}

/**
 * Updates a user, giving or revoking permissions.
 * @param study - The study in question
 * @param username - Name of the user to grant privileges to or revoke them from
 * @param level - Either "viewer", "contributor" (grants) or "revoke" ( removes).
 * @returns Response object
 */
export const addPhenoDataFromFile = async(study: string, filename: string, transpose: boolean) => {
    const update_form: FormData = new FormData();
    update_form.append('study_id', study);
    update_form.append('filename', filename);
    update_form.append('transpose', transpose ? "true" : "false");
    let response = await fetch(`${import.meta.env.VITE_BACKEND_PATH}AddPhenotypeDataFromFile`, {
      method: 'post',
      headers: generateHeaders(),
      body: update_form,
    })
    return response.json();
}

/**
 * Deletes a study.
 * @param study - The study in question
 * @returns Response object
 */
export const deleteStudy = async(study: string) => {
    const delete_form: FormData = new FormData();
    delete_form.append('study_id', study);
    let response = await fetch(`${import.meta.env.VITE_BACKEND_PATH}StudyDelete`, {
        method: 'post',
        headers: generateHeaders(),
        body: delete_form,
    })
    return response.json();
}

/**
 * Fetches experimental data for a given study from the backend, for download.
 * @param data - The data in question
 * @returns 200 if successful, else it shown an alert
 */
export const downloadExperimentalData = async(study_id: string, filename: string): Promise<Blob|null> => {
    let result = await fetch(`${import.meta.env.VITE_BACKEND_PATH}DownloadExperimentalData?` + new URLSearchParams({
        study_id: study_id,
        filename: filename
    }), {
        method: 'get',
        headers: generateHeaders(),
    }).then((res) => {
        if (res.status == 200) {
            return res.blob();
        } else {
            window.alert("Sorry, there was a problem downloading your raw data!");
            return null;
        }
    });
    return result;
}

/**
 * Deletes experimental data.
 * @param study - The study in question
 * @param filename - The file in question
 * @returns Response object
 */
export const deleteExperimentalData = async(study_id: string, filename: string) => {
    const delete_form: FormData = new FormData();
    delete_form.append('study_id', study_id);
    delete_form.append('filename', filename);
    let response = await fetch(`${import.meta.env.VITE_BACKEND_PATH}DeleteExperimentalDataFile`, {
        method: 'post',
        headers: generateHeaders(),
        body: delete_form,
    })
    return response.json();
}

/**
 * GETs data on the contained studies from flask microservice.
 * @returns JS Object
 */
export const getStudyExperimentalFilenames = async (study: string): Promise<any> => {
    if (!study) return {};
    let result = await fetch(`${import.meta.env.VITE_BACKEND_PATH}GetExperimentalDataFilenames?`  + new URLSearchParams({
        'study_id': study,
    }),
    {
        method: 'get',
        headers: generateHeaders('application/json')
    });
    if (result.status == 200) {
        return result.json();
    }
    return {};
}

export interface ParseParameters {
    conc_delimiter?: string,
    pheno_delimiter?: string,
    conc_transpose?: boolean,
    pheno_transpose?: boolean,
}
