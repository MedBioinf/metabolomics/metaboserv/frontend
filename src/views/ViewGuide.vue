<template>
    <div class="container">
        <div class="heading">User Guide</div>
        <div class="subheading">Logging in and out</div>
            <div>
            Click on the button on the top right to log in (or log out, if you are already logged in).<br>
            You can either use a MetaboSERV account, which you can register by clicking the link on the bottom of the login menu, or an authorization token that has been passed to you by a third party.
            If verification is turned on in the configuration options, you may need to verify your account before you unlock most permissions.
            A verification mail will be sent to you in this case, which will contain a code you need to enter once in the "Account" section after registering your account.<br>
            Both variants of authentication (user accounts and authorization tokens) will allow you to browse, download and in some cases edit a set of studies which were uploaded to MetaboSERV. If you use a MetaboSERV account, you may upload new studies, as well.
            </div>
        <div class="subheading">Creating/deleting/editing studies</div>
            <div>
            Navigate to the "Studies" tab and click the plus. Fill in the mandatory metadata values on the left. On the right side, you can add annotated files, which must adhere to the <a href="#format">format specifications</a> stated below. On the bottom right, you can also manually add some metadata entries.<br>
            You can tick the transpose option in order to swap rows and columns (refer to the <a href="#format">format specifications</a>).<br>
            You can edit studies at any time by clicking on a study in the "Studies" tab and clicking on the "Edit" button. You can also delete studies here if you wish.
            </div>
        <div class="subheading">Uploading/downloading experimental data</div>
            <div>
            In the "Studies" tab, click on a study and click on "Manage experimental data". To upload files, you can either click on the dropbox on the left to browse your machine for according files, or drag files into the box using drag-and-drop. Uploaded files appear on the right, where you can download or delete them (given the necessary permissions).
            </div>
        <div class="subheading">Managing study permissions</div>
            <div>
            Click on an existing study in the "Studies" tab. If you have the necessary permissions, you will be able to add new contributors, remove contributors (top left) and manage authorization tokens (top right) for the study. Otherwise, you will only be able to browse the contained phenotypes and metadata.
            </div>
        <div class="subheading">Queries</div>
            <div>
            You can query your studies by navigating to the "Query" tab. There are four distinct sections here, three of which open up after you selected at least one study in the <b>first section</b> ("Study Selection"). Any study you select here (indicated by being listed on the right) is included in your query.<br>
            <br>In the <b>second</b> section, you can optionally limit the amount of metabolites to consider in your query. By default, all metabolites are used. On the very top of the window, you can choose to only retrieve a selected subset of metabolites, or to retrieve all metabolites but the selected subset. Select metabolites by clicking on them in the list on the left.<br>
            <br>In the <b>third</b> section, you can optionally further limit which entries to consider by imposing filter rules on metabolites. There are generally three different filter rules. The first will cause your query to only include entries where the concentration level of the specified metabolite falls inside the range you provide. The second filter type does the same, but only considers entries where the concentration level falls outside of the provided range. The last only serves to visualize a particular range and will not change the query set.
            If reference concentrations are contained in the MetaboSERV instance, you can also select them here wherever possible instead of providing a numerical range by yourself.<br>
            <br>In the <b>fourth</b> section, you can choose which phenotypes should be included in your query. You can also limit the query set by the levels of each nominal phenotype, such as only considering healthy patients.
            </div>
        <div class="subheading">Creating quality control plots and heatmaps, visualizing Bruker NMR spectra</div>
            <div>
            MetaboSERV facilitates easy creation of quality control plots and Pearson correlation heatmaps for your uploaded concentration data. Navigate to "Browse" and select a study you want to create a quality control plot. Then select your desired plot, choose whether you want to create a plot from a &lt;LODor &gt;LOD perspective (for QC plots) and click on Submit.
            Bruker NMR spectra can be visualized here, as well. Simply select a study, then select the according archive from the dropdown list and click on "Execute". Note that the archive has to be uploaded using the "Manage experimental metadata" functionality of the study beforehand.
            </div>
        <div class="subheading">Mapping metabolites to their HMDB identifiers</div>
            <div>
                Navigate to the MetaboSERV landing page. The bottom line of the first section (right above "Citation") has a link that will take you to the mapping utility.
                Here, you can either map a list of metabolites to their HMDB equivalents or provide a file that follows the concentration data format specifications outlined below. Click on the "Map" button. For each metabolite (either taken from your provided list or provided file), the MetaboSERV database will be searched for a fitting HMDB identifier.<br>
                <br>All matches will have to be acknowledged and accepted by you, which you can do by clicking on the green tick button. Exact matches are accepted automatically. If you are dissatisfied with a particular mapping, you can browse through different matches for each metabolite by clicking on the < and > buttons. We use <a href="https://cts.fiehnlab.ucdavis.edu/">The Chemical Translation Service (CTS)</a> provided by the Fiehn Lab at UC Davis West Coast Metabolomics Center to try and resolve metabolites that were not matched at all in the MetaboSERV database. In case no match can be found using the MetaboSERV database or the CTS, which is indicated by the word Failed, you can still manually research and provide a correct identifier by clicking on the + button. You can also simply accept failed matches, which will result in the original metabolite name remaining and not being replaced by a HMDB identifier.
                Results can be downloaded as JSON. If you provided a concentration data file, you can also download the adapted version with all metabolites being replaced with their HMDB accession numbers as long as you accepted the mappings.
            </div>
        <div class="heading" id="format">Format Specifications</div>
            <div>For any data you upload, a particular format has to be adhered to in order for MetaboSERV to parse your files. The following sections describe the allowed formats in detail for each particular type of file, of which there are:</div>
            <ul>
                <li>Concentration data files, containing concentration levels of particular sources (e.g. patients) in your study</li>
                <li>Metadata files, containing general metadata for your study</li>
                <li>Phenotype files, containing nominal phenotype data for particular sources (e.g. patients) in your study</li>
                <li>Raw experimental data. This should be compressed/archived to save storage space, but otherwise no restrictions are placed on it.</li>
            </ul>
            Templates can be downloaded from the public MetaboSERV instance's landing page.
        <div class="subheading">Concentration data files</div>
            <div>
                Allowed file extensions are: CSV, TSV, XLS, XLSX.
                The standard layout for concentration data files is to have one column per metabolite, and one column containing according patient IDs. Each source, e.g. a patient, is then reflected in one row of the file.<br>
                <br>Please provide a header row. The column containing patient IDs should contain a header called <span class="code">id</span> or <span class="code">source_id</span>. All other columns are considered as metabolites, and whatever you pass in the header row is considered the metabolite name. In order to make the most out of MetaboSERV's querying features, such as combining different studies containing similar metabolites in a single query, it is optimal to provide HMDB accession numbers instead of metabolite names as those are guaranteed to be matched. However, any metabolite names are accepted unless HMDB identifiers are enforced in the relevant MetaboSERV instance.<br>
                <br>In addition to rows for each source, you may also pass a special row that indicates the unit used for each metabolite. Insert <span class="code">unit</span> in the <span class="code">source_id</span> column of the file for this row, and then provide the according unit for each metabolite in the respective columns. MetaboSERV places no restrictions on unit identifiers.<br>Refer to this exemplary table or to the provided exemplary files for clarification:
                <div class="example-table">
                    <div class="row">
                        <div><b>source_id</b></div>
                        <div><b>metabolite 1</b></div>
                        <div><b>metabolite 2</b></div>
                    </div>
                    <div class="row">
                        <div>unit</div>
                        <div>uL</div>
                        <div>mmol/L</div>
                    </div>
                    <div class="row">
                        <div>patient_1</div>
                        <div>3714</div>
                        <div>&lt;LOD</div>
                    </div>
                    <div class="row">
                        <div>patient_2</div>
                        <div>5815</div>
                        <div>279</div>
                    </div>
                </div>
                Each cell may contain either nothing, the special string <span class="code">&lt;LOD</span> (both treated as NAs) or a numerical value indicating the concentration value of the according source x for metabolite y.<br>
                <br>You can also swap rows and columns, in which case you need to tick the transpose option for the concentration file when creating your study.
            </div>
        <div class="subheading">Metadata files</div>
            <div>
                Allowed file extensions: YML, JSON.<br>
                You can provide a simple YML file to add some additional metadata for your study. This is particularly useful if you have large amounts of metadata. Otherwise, it is recommended to simply set it using the frontend GUI during study creation.<br>
                Each metadata entry is associated with a unique ID, a name and a value, passed in the format
                <pre>
                    <code>
    id:
        name: Name
        value: Value
                    </code>
                </pre>
                Example:
                <pre>
                    <code>
    instrument:
        name: Spectrometer
        value: 600 MHz Bruker Avance III 
    another_md:
        name: Another attribute
        value: Another value
                    </code>
                </pre>
                Or the JSON equivalent:
                <pre>
                    <code>
    {
        "instrument": {
            "name": "Spectrometer",
            "value": "600 MHz Bruker Avance III"
        },
        "another_md": {
            "name": "Another attribute",
            "value": "Another value"
        }
    }
                    </code>
                </pre>
                You can pass an arbitrary amount of entries this way.
            </div>
        <div class="subheading">Phenotype data files</div>
            <div>
                Allowed file extensions: CSV, TSV, XLS, XLSX. Allowed separators are tabs (<span class="code">\t</span>) and semicolons (<span class="code">;</span>). Even in CSV files, commas are currently not allowed in order not to interfere with the parsing of numerical data.<br>
                Generally, phenotype files are set up similarly to concentration data files. There are three types of possible columns:
                <ul>
                    <li>ID column (mandatory), which should be indicated by setting <span class="code">id</span> or <span class="code">source_id</span> in the header row. Add all source IDs, e.g. patient IDs, here.</li>
                    <li>Phenotype columns (at least one is mandatory). Indicate phenotype columns by adding <span class="code">PHENOTYPE_</span> before the phenotype description in the header row, e.g. <span class="code">PHENOTYPE_Acute Kidney Injury</span>. Each cell in this column should contain the phenotype level that the source (e.g. patient) in each row is associated with.</li>
                    <li>Metadata columns (optional). All columns which are not starting with <span class="code">PHENOTYPE_</span> or are marked as the ID column are treated as metadata. You can add arbitrary values into these columns</li>
                </ul>
                Refer to this exemplary table or the provided exemplary files for clarification:<br>
                <div class="example-table">
                    <div class="row">
                        <div><b>source_id</b></div>
                        <div><b>PHENOTYPE_some disease</b></div>
                        <div><b>PHENOTYPE_something else</b></div>
                        <div><b>collection time</b></div>
                    </div>
                    <div class="row">
                        <div>patient_1</div>
                        <div>healthy</div>
                        <div>level 1</div>
                        <div>17:37pm</div>
                    </div>
                    <div class="row">
                        <div>patient_2</div>
                        <div>sick</div>
                        <div>level 1</div>
                        <div>11:17am</div>
                    </div>
                    <div class="row">
                        <div>patient_3</div>
                        <div>healthy</div>
                        <div>level 2</div>
                        <div></div>
                    </div>
                </div>
            </div>
        <div class="subheading">Raw experimental data</div>
            <div>
                MetaboSERV sets no limitations about the data you choose to upload to a study, i.e. all types of files are allowed. It is up to the user to make sure no illegal content is uploaded. However, we recommend compressing larger files in order to reduce the storage space allocated to your study.
            </div>
        <div class="heading" style="margin-top: 1rem;">Administrator Guide</div>
        <div class="subheading">Installation</div>
            <div>
            For the complete installation guide for MetaboSERV, please refer to the <a href="https://gitlab.gwdg.de/metaboserv2/backend/-/blob/main/README.md">GitLab repository</a>, where it is described in detail.
            There is also a video guide for installing MetaboSERV on Windows 11:
            <iframe width="560" height="315" src="https://www.youtube.com/embed/vbqY2qJcgLk?si=veExp5dyDg5_0qrQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
            </div>
    </div>

</template>

<script setup lang="ts">
import { onMounted } from 'vue';
import { useStudyStore } from '@/stores/studies';
import { areHMDBIdentifiersEnforced } from '@/modules/query_module';

const studies = useStudyStore();

onMounted(async () => {
    if (studies.hmdb_enforced === null) {
        let enforced = await areHMDBIdentifiersEnforced();
        studies.hmdb_enforced = enforced.body as boolean;
    }
})

</script>

<style scoped>

.container {
    display: flex;
    justify-items: baseline;
    flex-direction: column;
    min-width: 65%;
    max-width: 90%;
    margin: 2rem auto;
    font-size: var(--fs-default);
    gap: 1rem;
}
.heading {
    font-size: 1.3rem;
    font-weight: 600;
    color: var(--color-head);
}

.subheading {
    font-size: var(--fs-default);
    font-weight: 600;
    color: var(--color-head);
    
}

.subsubheading {
    font-size: 1rem;
    font-weight: 600;
}

a, a:active {
    color: var(--color-primary);
}

a:hover {
    color: var(--color-head);
}

.contact {
    display: grid;
    grid-template-columns: 0.75fr 1fr;
    grid-auto-flow: row;
}

.code {
    font-family: monospace;
}

.example-table {
    display: grid;
    grid-template-columns: 1fr;
    grid-auto-flow: row;
    gap: 1rem;
    font-size: var(--fs-small);
    border: 1px solid var(--color-primary);
    padding: 0.5rem;
    max-width: 70vw;
}

.row {
    display: grid;
    grid-template-columns: repeat(auto-fill, 30ch);
    grid-auto-flow: column;
    gap: 2rem;
    width: 100%;
    text-align: left;
}

</style>