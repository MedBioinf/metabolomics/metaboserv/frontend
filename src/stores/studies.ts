import type { TokenData } from '@/modules/auth_module';
import { defineStore } from 'pinia'

export const useStudyStore = defineStore('studies', {
  state: () =>  ({
    studies_owned: null as StudyData[] | null,
    studies_shared: null as StudyData[] | null,
    studies_public: null as StudyData[] | null,
    hmdb_enforced: null as boolean|null,
  }),
  actions: {
    distributeStudies(s_owned: StudyData[] | null, s_shared: StudyData[] | null, s_public: StudyData[] | null) {
      this.studies_owned = s_owned;
      this.studies_shared = s_shared;
      this.studies_public = s_public; 
    },
    clearStudies() {
      this.studies_owned?.splice(0, this.studies_owned.length);
      this.studies_shared?.splice(0, this.studies_shared.length);
      this.studies_public?.splice(0, this.studies_public.length);
    },
  },
});

export interface StudyData {
  study_id: string,
  study_type: string,
  study_specimen: string,
  study_name: string,
  study_compounds?: string[],
  study_visibility: string,
  study_author: string,
  study_metadata: MetadataObject | null,
  study_members: object,
  study_date?: string,
  study_phenotypes?: string[] | null,
  study_auth_tokens?: {
    [key: string]: TokenData,
  },
}


export interface MetadataObject {
  [key: string]: any,
}

export interface StudyFiles {
  concentration_file: File | null,
  phenotype_file: File | null,
  metadata_file: File | null,
}

export const enum StudyModalStatus {
  Closed = "closed",
  View = "view",
  Edit = "edit"
}
