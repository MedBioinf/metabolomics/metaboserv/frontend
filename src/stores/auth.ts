import { defineStore } from 'pinia'

export const useAuthStore = defineStore('auth', {
  state: () =>  ({
    jwt: null as string | null, //JSON Web Token
    aut: null as string | null, //Authorization Token
    username: "" as string, //Username
  }),
});

