import { createRouter, createWebHistory } from 'vue-router';

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/browse',
      name: 'browse',
      component: () => import('@/views/ViewBrowse.vue'),
    },
    {
      path: '/query',
      name: 'query',
      component: () => import('@/views/ViewQuery.vue'),
    },
    {
      path: '/studies',
      name: 'study',
      component: () => import('@/views/ViewStudy.vue'),
    },
    {
      path: '/',
      name: 'home',
      component: () => import('@/views/ViewHome.vue'),
    },
    {
      path: '/account',
      name: 'account',
      component: () => import('@/views/ViewAccount.vue')
    },
    {
      path: '/about',
      name: 'about',
      component: () => import('@/views/ViewAbout.vue')
    },
    {
      path: '/map',
      name: 'map',
      component: () => import('@/views/ViewMapToHMDB.vue')
    },
    {
      path: '/help',
      name: 'guide',
      component: () => import('@/views/ViewGuide.vue')
    }
  ],
  scrollBehavior (to) {
    if (to.hash) {
      return {el: to.hash}
    }
  }
});

export default router;
